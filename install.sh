#!/bin/bash

# add pythonpath if isn't already there

custompymodulepath='$LIB_DIR/py'
custompymodulepathexpanded="$LIB_DIR/py"
if [ "${PYTHONPATH#*$custompymodulepath}" == "$PYTHONPATH" ]; then
	if [ "${PYTHONPATH#*$custompymodulepathexpanded}" == "$PYTHONPATH" ]; then  
		echo "export PYTHONPATH=\${PYTHONPATH}:$custompymodulepath" >> ~/.env
		bash ~/.env
	fi
fi
